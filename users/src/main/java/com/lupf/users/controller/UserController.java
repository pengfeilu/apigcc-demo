package com.lupf.users.controller;

import com.lupf.users.beans.UserInfoVo;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 用户管理
 */
@RestController
@RequestMapping("user")
public class UserController {

    /**
     * 获取所有用户
     * @return 用户列表
     */
    @GetMapping("all")
    public List<UserInfoVo> getAllUser(){
        return null;
    }
}
