package com.lupf.order;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
@RunWith(SpringRunner.class)
public class OrderApplicationTests {

	@Autowired
	StringRedisTemplate stringRedisTemplate;

	@Test
	public void contextLoads() {
		String[] a = new String[1000];
		for (int i = 1000; i < 2000; i++) {
			a[i] = String.valueOf(i);
		}
		stringRedisTemplate.opsForSet().add("zs1",a);
	}

}
