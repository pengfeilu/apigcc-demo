package com.lupf.order.controller;

import com.lupf.order.bean.OrderInfoRo;
import com.lupf.order.bean.OrderInfoVo;
import org.springframework.web.bind.annotation.*;

/**
 * 订单管理
 */
@RestController
@RequestMapping("/order")
public class OrderController {

    /**
     * 根据单号获取的订单
     * @param orderNo 订单号
     * @return 订单对象
     */
    @GetMapping("get")
    public OrderInfoVo getOrder(@RequestParam(defaultValue = "2020111111") String orderNo){
        return null;
    }

    /**
     * 创建订单
     * @param orderInfo 订单信息
     * @return 是否添加成功
     */
    @PostMapping("add")
    public Integer add(@RequestBody OrderInfoVo orderInfo){
        return 1;
    }

    /**
     * 修改订单
     * @param orderInfo 修改的订单信息
     * @return 是否修改成功
     */
    @PostMapping("update")
    public Integer update(@RequestBody OrderInfoRo orderInfo){
        return 1;
    }
}
