package com.lupf.product.controller;

import com.lupf.product.beans.ProductInfo;
import com.lupf.product.beans.ProductInfoVo;
import org.springframework.web.bind.annotation.*;

/**
 * 商品服务
 */
@RestController
@RequestMapping("product")
public class ProductController {

    /**
     * 获取商品名称
     * @param id 商品id
     * @return 商品名称
     */
    @GetMapping("getbyid")
    public String getProductNameById(Long id){
        return "iphone 8";
    }

    /**
     * 添加商品
     * @param productInfo 商品对象
     * @return 添加后的商品信息
     */
    @PostMapping("add")
    public ProductInfoVo add(@RequestBody  ProductInfo productInfo){
        return null;
    }
}
